Sequel.migration do
  up do
    add_column :containers, :network, String
  end
  down do
    drop_column :containers, :network
  end
end
