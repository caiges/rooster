Sequel.migration do
  up do
    create_table :containers do
      primary_key :id
      String :name
      String :image 
      String :image_tag
      String :tag
      TrueClass :active
    end
  end
  down do
    drop_table :containers
  end
end
