Sequel.migration do
  up do
    alter_table :containers do
      set_column_default :name, nil
      set_column_default :image_tag, "latest"
      set_column_default :tag, nil
      set_column_default :ports, nil
      set_column_default :active, false
      set_column_default :tags, nil
      set_column_default :labels, nil
      set_column_default :env_file, nil
      set_column_default :network, nil
      set_column_default :image_pull_secret, nil
    end
  end
end
