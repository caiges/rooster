Sequel.migration do
  up do
    add_column :containers, :ports, String, text: true
  end
  down do
    drop_column :containers, :ports
  end
end
