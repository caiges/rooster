Sequel.migration do
  up do
    create_table :agents do
      primary_key :id
      foreign_key :container_id, :containers
    end
  end
  down do
    drop_table :agents
  end
end
