Sequel.migration do
  up do
    add_column :containers, :labels, String, text: true
  end
  down do
    drop_column :containers, :labels
  end
end
