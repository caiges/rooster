Sequel.migration do
  up do
    add_column :containers, :env_file, String
  end
  down do
    drop_column :containers, :env_file
  end
end
