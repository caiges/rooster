Sequel.migration do
  up do
    add_column :containers, :image_pull_secret, String
  end
  down do
    drop_column :containers, :image_pull_secret
  end
end
