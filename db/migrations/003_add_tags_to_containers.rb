Sequel.migration do
  up do
    add_column :containers, :tags, String
  end
  down do
    drop_column :containers, :tags
  end
end
