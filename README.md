# Rooster - Manage containers across hosts with nothing but a Rooster.

![image](/rooster.png)

Rooster has two components, server and agent, which together manage containers across a fleet of hosts.

# Server

The server is both an API and UI for managing containers. You can create containers, set new image tags and control which hosts should be running which containers.

## Developing

- Install Ruby 3.2.
- `bundle install`
- Copy `.env.example` to `.env`.
- `make watch`

# Agent

The agent communicates with the server and manages the containers running on a given host.

Host tags, not to be confused with image tags, are simple descriptors which identify that an agent running on a host is interested in running and managing containers that share the same host tag. When a container is created in the Rooster UI, you configure both the `<image>:<tag>` and the host tag. For example, if you define a container in Rooster, `nginx:alpine` with a host tag of `web`, and start an agent on a host with a tag of `web`, `nginx:alpine` will run on that host.

Agent configuration is simple, tell it which Rooster server it should talk to and what its host tags are.

## Developing

- Install Ruby 3.2.
- `cd agent`
- `bundle install`
- `make watch-agent`

