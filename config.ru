Encoding.default_internal = Encoding.default_external = 'UTF-8' if RUBY_VERSION >= '1.9'

require 'dotenv/load'

require ::File.expand_path('rooster', __dir__)
run RoosterApp.app
