require "securerandom"
require "roda"
require "forme"
require "docker_registry2"
require_relative "db"

# App is our main application class.
class RoosterApp < Roda
  plugin :render, escape: true
  plugin :sessions, secret: SecureRandom.hex(100)
  plugin :forme
  plugin :route_csrf
  plugin :flash
  plugin :link_to
  plugin :all_verbs
  plugin :type_routing
  plugin :public
  plugin :request_headers

  route do |r|
    # Public assets
    r.public

    # GET / request
    r.root do
      r.redirect "/containers"
    end

    # /containers branch
    #    r on 'containers' do

    # GET /containers/tags
    #      r.get 'tags' do
    #        @tags = r.params[:tags].split(',')
    #        @specs = Container.where(:tags, )
    #      end
    #    end

    # /containers branch
    r.on "containers" do
      @title = "containers"

      # GET /containers/new
      r.get "new" do
        @container = Container.new
        @container.active = true
        view "containers/new"
      end

      # GET /containers/tags
      r.get "tags" do
        tags = r.params.fetch("tags", "").split(",")
        containers = Container.where :tag => tags
        r.json do
          containers.to_json
        end
      end

      # GET /container/:id
      r.on Integer do |id|
        @container = Container.first(id: id)

        # GET /containrs/:id/tags
        r.on "tags" do
          #container_image = (@container.image.include? ".") ? @container.image : "registry.hub.docker.com/#{@container.image}"
          registry = ""
          repo = @container.image
          if @container.image.include? "."
            registry, repo = @container.image.split("/", 2)
            registry = "https://#{registry}"
          else
            # Assume we're using a library image from Docker Hub.
            registry = "https://registry.hub.docker.com"
            repo = "library/#{@container.image}"
          end

          registry_credentials = nil
          unless @container.image_pull_secret.nil?
            begin
              image_pull_secret = JSON.parse(@container.image_pull_secret)
              registry_credentials = {
                user: image_pull_secret["username"],
                password: image_pull_secret["password"],
              }
            rescue JSON::ParserError => e
              registry_credentials = nil
            end
          end

          if registry_credentials.nil?
            registry = DockerRegistry2.connect(registry)
          else
            registry = DockerRegistry2.connect(registry, registry_credentials)
          end

          @tags = registry.tags(repo)["tags"]

          r.html do
            render "containers/_tags"
          end

          r.json do
            @tags.to_json
          end
        end

        # GET /containers/:id/edit
        r.on "edit" do
          if r.headers["HX-Request"]
            render "containers/edit"
          else
            view "containers/edit"
          end
        end

        # POST /containers/:id/delete
        r.post "delete" do
          if @container.destroy
            flash[:success] = "Container deleted"
          else
            flash[:error] = "There was a problem deleting the container"
          end

          response.headers["HX-Redirect"] = "/containers"
        end

        # GET /containers/:id
        r.get do
          r.html do
            view "containers/show"
          end
          r.json do
            @container.to_json
          end
        end

        # PUT /containers/:id
        r.put do
          @container.update r.params["container"]
          if @container.save
            render "containers/show"
          else
            render "containers/edit"
          end
        end
      end

      # GET /containers request
      r.is do
        r.get do
          @containers = Container.all
          view "containers/index"
        end

        # POST /containers
        r.post do
          @container = Container.new r.params["container"]
          if @container.save
            flash[:success] = "Container #{@container.name} created"
            r.redirect "/containers"
          else
            flash[:error] = "There was a problem creating the container"
          end
        end
      end
    end
  end

  freeze
end
