require "faraday"
require "json"
require "yaml"
require "docker"
require "semantic_logger"

module RoosterAgent

  # Hold a reference to last applied configuration.
  @@last_applied = Hash.new

  def self.last_applied=(applied)
    @@last_applied = applied
  end

  def self.last_applied
    @@last_applied
  end

  def self.start(server, tags, debug)
    SemanticLogger.default_level = :trace
    SemanticLogger.add_appender(io: $stderr, level: :trace)
    logger = SemanticLogger["RoosterAgent"]

    loop do
      logger.info "checking the henhouse #{server} using tags: #{tags}"
      begin
        containers = fetch_container_definitions(server, tags)
      rescue
        logger.error "error fetching container definitions"
        sleep 3
        next
      end

      # Process deleted containers.
      begin
        prune_containers(containers, RoosterAgent.last_applied)
      rescue StandardError => e
        logger.error "container could not be removed: #{e}"
      end

      containers.each do |container_definition|

        # Authenticate to registry if image_pull_secret is set.
        unless container_definition[:image_pull_secret].empty?
          begin
            image_pull_secret = JSON.parse(container_definition[:image_pull_secret])
            begin
              Docker.authenticate!(image_pull_secret)
            rescue Docker::Error::AuthenticationError => e
              logger.error "error authenticating to registry: #{e}"
              sleep 3
              next
            end
          rescue JSON::ParserError => e
            logger.error "error parsing image pull secret: #{e}"
            sleep 3
            next
          end
        end

        begin
          container_spec = ContainerSpec.from_definition(container_definition)
        rescue StandardError => e
          logger.error "error creating container spec: #{container_definition}: #{e}"
          sleep 3
          next
        end

        # Pull image
        begin
          container_spec.pull
        rescue Docker::Error::ServerError => e
          logger.error "error pulling image #{container_spec.image}: #{e}"
          sleep 3
          next
        end

        begin
          # This will throw an exception if not found.
          container = get_container container_spec.name
          container.remove(force: true) if !container_spec.active || container_spec.different_from_container?(container) || container.info["State"]["Running"] == false
        rescue
          logger.info "container \"#{container_spec.name}\" not running"

          #begin
          # Create container
          new_container = container_spec.create
          # Start container
          logger.info "starting container #{container_spec.name} as #{new_container.id}"
          new_container.start
          container = get_container container_spec.name
          #rescue
          #  logger.error "container \"#{container_spec.name}\" could not be started"
          #end
        end
      end

      sleep 3
    end
  end

  def self.get_container(name)
    Docker::Container.get(name)
  end

  def self.fetch_container_definitions(server, tags)
    response = Faraday.get("#{server}/containers/tags", { tags: tags }, { "Accept" => "application/json" })
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.prune_containers(containers, last_applied)
    container_names = containers.map { |c| c[:name] }
    last_applied_container_names = last_applied.keys

    containers_to_remove = last_applied_container_names - container_names
    containers_to_remove.each do |c|
      container = get_container c
      container.remove(force: true)
    end
  end
end

class ContainerSpec
  attr_accessor :image, :image_id, :image_reference, :name, :labels, :network, :ports, :env, :env_file, :active

  def pull
    @image_reference = Docker::Image.create("fromImage" => image)
    @image_id = image_reference.id
  end

  def create
    container_opts = {
      "name" => name,
      "Image" => image,
      "Labels" => labels,
      "Env" => env,
      "ExposedPorts" => {},
      "HostConfig" => {},
    }

    unless ports.empty?
      container_opts["HostConfig"]["PortBindings"] = {}
      ports.each do |k, v|
        if k.include?(":")
          host_port, port_protocol = k.split(":")
          port, protocol = port_protocol.split("/")
          container_opts["HostConfig"]["PortBindings"][port_protocol] = [{
            "HostPort" => host_port,
          }]
          container_opts["ExposedPorts"][port_protocol] = {}
          puts "exposed ports #{container_opts["ExposedPorts"]}"
        else
          port, protocol = k.split("/")
          #container_opts["HostConfig"]["PortBindings"][port_protocol] = [{}]
          puts "k #{k}"
          container_opts["ExposedPorts"][k] = {}
          puts "exposed ports #{container_opts["ExposedPorts"]}"
        end
      end
    end

    puts "network #{network}"
    # todo: bodge networking in here for now
    unless network.empty?
      begin
        existing_network = Docker::Network.get(network)
      rescue Docker::Error::NotFoundError
        existing_network = Docker::Network.create(network, { "Driver" => "bridge" })
      end

      container_opts["NetworkingConfig"] = {
        "EndpointsConfig" => {
          "#{network}" => {
            "NetworkID" => existing_network.id,
          },
        },
      }
    end

    RoosterAgent.last_applied["#{name}"] = { labels: labels, env: env, network: network, ports: ports }
    Docker::Container.create(container_opts)
  end

  def different_from_container?(running_container)
    return true if image_id != running_container.info["Image"]

    # Ensure labels match. If agent has no known last applied label configuration, it will report this as a difference and recreate the container.
    last_applied = RoosterAgent.last_applied[@name]
    return true if last_applied.nil?
    return true if last_applied.fetch(:labels, {}) != @labels
    return true if last_applied.fetch(:env, {}) != @env
    return true if last_applied.fetch(:ports, {}) != @ports
    return true if last_applied.fetch(:network, nil) != @network

    false
  end

  def self.from_definition(definition)
    spec = ContainerSpec.new
    spec.active = definition[:active]
    spec.name = definition[:name]
    spec.image = "#{definition[:image]}:#{definition[:image_tag]}"
    spec.labels = definition[:labels].split("\n").map { |l| l.split("=") }.to_h
    spec.env_file = definition[:env_file]
    spec.env = definition[:env_file].empty? ? [] : File.read(definition[:env_file]).split()
    spec.ports = definition.fetch(:ports, []).split().each_with_object({}) { |port, ports| ports[port] = {} }
    spec.network = definition[:network]
    spec
  end
end
