Gem::Specification.new do |s|
  s.name        = 'rooster-agent'
  s.version     = '0.0.0'
  s.summary     = 'Hola!'
  s.description = 'A simple hello world gem'
  s.authors     = ['Caige Nichols']
  s.email       = 'caigesn@gmail.com'
  s.files       = ['lib/rooster_agent.rb']
  s.executables << 'rooster-agent'
  s.homepage =
    'https://rubygems.org/gems/rooster-agent'
  s.license = 'MIT'
  s.add_runtime_dependency 'optimist', '~> 3.1'
  s.add_runtime_dependency 'faraday', '~> 2.7'
  s.add_runtime_dependency 'docker-api', '~> 2.2'
  s.add_runtime_dependency 'semantic_logger', '~> 4.14.0'
end
