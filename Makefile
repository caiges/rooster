watch:
	bundle exec rerun puma

db-shell:
	bundle exec sequel sqlite://rooster.db

watch-agent:
	cd agent && bundle exec rerun -- ruby -Ilib/ bin/rooster-agent -s http://localhost:9292 -t web -d

image:
	docker buildx build --platform linux/amd64 --push -t caiges/rooster .

push:
	docker push caiges/rooster
	docker push caiges/rooster-agent

agent-image:
	cd agent && docker buildx build --platform linux/amd64 --push -t caiges/rooster-agent .
