require 'sequel'
require 'sqlite3'

# Load the migration extension.
Sequel.extension :migration
# Enable JSON serializer for models.
Sequel::Model.plugin :json_serializer

# Connect to the database.
db = Sequel.connect(ENV['DATABASE_URL'])

# Run migrations.
Sequel::Migrator.run(db, 'db/migrations')

# Enable forme plugin.
Sequel::Model.plugin :forme

class Container < Sequel::Model; end

class Agent < Sequel::Model; end
